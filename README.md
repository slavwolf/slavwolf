## Hello there!

### I'm a DevOps Engineer from Poland.

#### PROJECTS
I organize all my projects in [groups]

- [SubsPlease RSS Helper] - Scripts for getting info about new episodes from SubsPlease RSS channel
- [Xboxdrv Brook Zero Pi] - Support for Brook Zero-Pi fightstick board in [xboxdrv]

#### ABOUT ME

- I'm scripting in Python and Bash
- I use daily: `terraform`, `docker`, `k8s`, `helm`, `prometheus`, `grafana`
- Dogs > cats

#### COOL STUFF
- [hypermodern-python]

[groups]: https://gitlab.com/users/slavwolf/groups
[SubsPlease RSS Helper]: https://gitlab.com/slavwolf-projects/python/subsplease-rss
[Xboxdrv Brook Zero Pi]: https://gitlab.com/slavwolf-forks/xboxdrv-brook-zero-pi
[xboxdrv]: https://xboxdrv.gitlab.io/
[hypermodern-python]: https://github.com/cjolowicz/cookiecutter-hypermodern-python
